# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

User.destroy_all
Account.destroy_all
TodoList.destroy_all
TodoItem.destroy_all
#Tag.destroy_all


User.create [
                {login: "Matt", password: "abc123"},
                {login: "Tim", password: "abc123"}
            ]


User.first.create_account({first_name: "Kalman", last_name: "Hazin", age: 33, gender: "male"})

User.last.create_account({first_name: "Mary", last_name: "Hartman", age: 52, gender: "female"})

User.first.todo_lists.create [
                                 {list_name: "Buy Tickets", list_due_date: Date.strptime("12/24/2018", "%m/%d/%Y")},
                                 {list_name: "Vacation", list_due_date: Date.strptime("06/14/2017", "%m/%d/%Y") }
                             ]

#add seed data for user named Tim
User.last.todo_lists.create [
                                {list_name: "Reserve Hotel", list_due_date: Date.strptime("04/22/2022", "%m/%d/%Y")},
                                {list_name: "Business Trips", list_due_date: Date.strptime("07/19/2019", "%m/%d/%Y") }
                            ]
#add seed data for user named Tim
User.last.create_account({first_name: "Tim", last_name: "Schulz", age: 22, gender: "male"})



User.first.todo_lists.first.todo_items.create [
                                                  {task_title: "Buy Tickets", description: "Purchase tickets for the movie", due_date: Date.strptime("12/24/2018", "%m/%d/%Y")},
                                                  {task_title: "Buy Tickets", description: "Buy more AMC tickets", due_date: Date.strptime("12/26/2018", "%m/%d/%Y")},
                                                  {task_title: "Vacation", description: "Vacationing in Hawaii", due_date: Date.strptime("06/14/2017", "%m/%d/%Y")}
                                              ]

#Will add seed data for user named Tim
User.last.todo_lists.first.todo_items.create [
                                                 {task_title: "Business Trips", description: "Buy plane tickets to meeting in New York", due_date: Date.strptime("12/26/2018", "%m/%d/%Y")},
                                                 {task_title: "Business Trips", description: "Business Meeting in Florida", due_date: Date.strptime("06/14/2017", "%m/%d/%Y")}
                                             ]

#create tags to associate with todo_items
#tag1 = Tag.create({tag_name: "business trips"})
#tag2 = Tag.create({tag_name: "vacations"})
#tag3 = Tag.create({tag_name: "business trips"})


#User.first.todo_lists.first.todo_items.first.tags << tag1
#User.first.todo_lists.first.todo_items.last.tags << tag2

#Will add seed data for user named Tim
#User.last.todo_lists.first.todo_items.first.tags << tag1
#User.last.todo_lists.first.todo_items.last.tags << tag3

#add account data for user named Matt
user = User.find_by_login("Matt")
user.create_account({first_name: "Matt", last_name: "Hassleback", age: 36, gender: "male"})
user.save
