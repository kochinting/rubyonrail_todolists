
#605.484 Summer 2016
#Team5-summer16
#Members: Sajjad Syed, Chin-Ting Ko
#Assignment 8

#Filename: assignment.rb
#contains queries and searches of the database

require_relative '../config/environment'

puts "\nIn assignment.rb testing assignment8"
puts "\n\nDisplay all the entries in the User table:"
puts User.all.as_json


puts "\nSpecific user rows retrieves"
puts User.last.account.last_name
puts User.last.account.first_name



#-----CHECKING VALIDATORS-----------------------------------------------
#Check age validator in account.rb. Age should be between 20 and 100
puts "\nCheck Validators in Account"
user = User.find_by_login("JohnW")
puts "\nlogin: " + user.login + " and password: " + user.password_digest

puts "Checking validator in account when age is not in the range of 20 to 100"
user.create_account({first_name: "John", last_name: "WickW", age: 76, gender: "gheh"})
user.save

user = User.find_by_login("JohnW")
puts user.account.as_json


puts "\nChecking vaidator in account when gender is not male, female, N/A"
user.create_account({first_name: "John", last_name: "WickW", age: 16, gender: "male"})
user.save

user = User.find_by_login("JohnW")
puts user.account.as_json

puts "user account NOT CREATED becuase of Validators"






#-----Check for Retrieve and Update-------------------------------------
puts "\nCheck for Retrieves and Updates of the Rows"

user = User.find_by_password_digest("ric899")

puts "\nlogin: " + user.login + " and password: " + user.password_digest

#generate random string and append it to old login
#so to get unique new login as a test
samplestr = [*('A'..'Z')].sample(3).join
user.update(login: "RichardV" + samplestr)
user.save
puts "New login: " + user.login + " and password: " + user.password_digest



#----Retrieve TodoItems from TodoList of the user TodoItems are displayed
# in ascending order as required by the assignment
puts"\nRetrieves TodoItems from TodoList of the user."
puts "TodoItems are displayed in ascending order as required by the assignment:"

puts User.first.todo_lists.first.todo_items.as_json



#-----Now Check for assignment 6 step 4: In addition to being able to get
# to TodoItems from TodoLists, map TodoItems directly on
# the User model
puts "\nCheck for #4 User mapped TodoItems, direct access:"
puts "Also display them in Ascending order"

puts User.first.each_list_todo_items.as_json


#----Retrieve Tags associated with TodoItems
puts"\nRetrieves Tags associated with TodoItems of the user."

puts User.first.todo_lists.first.todo_items.first.tags.as_json



