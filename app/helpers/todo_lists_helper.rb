module TodoListsHelper
  def all_done?(todo_list)
    return true if todo_list.todo_items.empty?
    todo_list.todo_items.all?(&:done)
  end

  def due_date_overall_class(todo_list)
    all_done?(todo_list) ? 'task_done' : 'task_not_done'
  end

  def update_attributes(attributes)
    self.attributes = attributes
    save
  end

end

