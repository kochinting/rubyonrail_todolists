json.array!(@todo_lists) do |todo_list|
  json.extract! todo_list, :id, :list_due_date, :list_name
  json.url todo_list_url(todo_list, format: :json)
end
