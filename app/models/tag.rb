class Tag < ActiveRecord::Base
  has_and_belongs_to_many :todo_items

  #validate :tag_entries


  #Validator since the requirement states: There should also be a
  # Tag model, which will have just one field – tag_name. Tags
  # will be things like “business trips” or	“vacations”.
  #def tag_entries
  #  if (tag_name != "business trips") || (tag_name != "vacations")
  #    errors.add(:tag_name, tag_name + " is not specified as business trips or as vacations")
  #  end
  #end


end
